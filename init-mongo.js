// Authenticate as the root user
db.getSiblingDB('admin').auth(
    process.env.MONGO_INITDB_ROOT_USERNAME,
    process.env.MONGO_INITDB_ROOT_PASSWORD
);

// Create a user with readWrite role in the specified database
db.createUser({
    user: process.env.MONGO_USER,
    pwd: process.env.MONGO_PASSWORD,
    roles: [
        { 
            role: "readWrite", 
            db: process.env.MONGO_INITDB_DATABASE
        },
    ]
});

// Switch to the wgui database and create collections
db.getSiblingDB(process.env.MONGO_INITDB_DATABASE).createCollection('clients');
db.getSiblingDB(process.env.MONGO_INITDB_DATABASE).createCollection('server');
db.getSiblingDB(process.env.MONGO_INITDB_DATABASE).createCollection('users');
db.getSiblingDB(process.env.MONGO_INITDB_DATABASE).createCollection('wake_on_lan_hosts');
